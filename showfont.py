import sys
import os.path
import tkinter
from math import sin

#
# Einstellungen
#
scale = 2               # Vergrößerungsfaktor für die Anzeige
winwidth = 256          # Fenstergröße x (nicht ändern)
winheight = 128         # Fenstergröße y (nicht ändern)
backcolor = '#000000'   # Hintergrundfarbe
forecolor = '#cccccc'   # Vordergrundfarbe
#
# Funktion Font-File einlesen
#
def getFont():
    # Prüfung Kommandozeilenparameter Font-File
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        # wenn Font-File existiert
        if os.path.isfile(filename) :
            # Font-File als Byte-Array einlesen
            font = open(sys.argv[1], "rb").read()
        else:
            print('Das Font-File "'+filename+'" existiert nicht.')
            sys.exit(2)
    else:
        print('Es muss ein Font-File als Parameter übergeben werden.')
        sys.exit(1)
    return font

#
# Font-File auf dem Bildschirm ausgeben
#
def showFont(font):
    #
    # gibt einen Pixel mit Scalierung aus
    #
    def putPixel(char, row, col, scale):
        if char in range(32):
            y = (0 + row) * scale
            x = (char * 8 + col) * scale
        if char in range(32, 64):
            y = (16 + row) * scale
            x = ((char - 32) * 8 + col) * scale
        if char in range(64, 96):
            y = (32 + row) * scale
            x = ((char - 64) * 8 + col) * scale
        if char in range(96, 128):
            y = (48 + row) * scale
            x = ((char - 96) * 8 + col) * scale
        if char in range(128, 160):
            y = (64 + row) * scale
            x = ((char - 128) * 8 + col) * scale
        if char in range(160, 192):
            y = (80 + row) * scale
            x = ((char - 160) * 8 + col) * scale
        if char in range(192, 224):
            y = (96 + row) * scale
            x = ((char - 192) * 8 + col) * scale
        if char in range(224, 256):
            y = (112 + row) * scale
            x = ((char - 224) * 8 + col) * scale
        for py in range(scale):
            for px in range(scale):
                img.put(forecolor, (x + px, y + py))

    # TK Fenster öffnen
    master = tkinter.Tk()
    w = tkinter.Canvas(master, width=winwidth*scale, height=winheight*scale, bg=backcolor, bd=0)
    w.pack()
    img = tkinter.PhotoImage(width=winwidth*scale, height=winheight*scale)
    w.create_image((winwidth*scale/2, winheight*scale/2), image=img, state='normal')

    # durch den Zeichensatz loopen
    for char in range(256):
        for row in range(16):
            line = font[char * 16 + row]
            if line & 1:
                putPixel(char, row, 7, scale)
            if line & 2:
                putPixel(char, row, 6, scale)
            if line & 4:
                putPixel(char, row, 5, scale)
            if line & 8:
                putPixel(char, row, 4, scale)
            if line & 16:
                putPixel(char, row, 3, scale)
            if line & 32:
                putPixel(char, row, 2, scale)
            if line & 64:
                putPixel(char, row, 1, scale)
            if line & 128:
                putPixel(char, row, 0, scale)

    tkinter.mainloop()

# Font-File laden
font = getFont()
showFont(font)
