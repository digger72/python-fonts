Dieses kleine Python-Script zeigt alte DOS-Zeichensätze im Format 8x16 als Grafik im Fenster an.
Ursprünglich wurden die Fonts mit einem Editor unter MS-DOS erstellt.

Version 1.0

Installation
------------
Es ist keine Installation nötig, das Script einfach mit einer Font-Datei als Parameter starten.

showfont.py ITALIC.ZEI

Abhängigkeiten
--------------
Python 3.0 ist Voraussetzung.
Das Script ist lauffähig unter Windows und Linux und es gibt keine weiteren Abhänigkeiten als "tkinter" welches aber per Default bei den meisten Python-Installation dabei ist.

Kontakt
-------
Fragen, Anregungen Kritik bitte an digger@barum.de